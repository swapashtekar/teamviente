import numpy as np
from sklearn import svm
import cPickle
import time

startTime = time.time()

data= np.genfromtxt("part-r-00000", delimiter="\t")
labels = data[:,-1]
data= np.delete(data, (-1),axis =1)     #to delete the labels
#data = np.delete(data, (-1), axis=1)    #to delete the f t values
data= np.delete(data, (0),axis=1)       #to delete the listing id
print data.shape

clf=svm.SVC()
clf.fit(data, labels)
with open('my_SVM_file.pkl', 'wb') as fid:
    cPickle.dump(clf, fid)

#testdata= np.genfromtxt("AustinDataChanged.csv", delimiter=",")
#print clf.predict(testdata)

print ("**** %s seconds ****" %(time.time() - startTime))
