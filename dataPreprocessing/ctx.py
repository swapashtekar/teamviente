import csv


#csvFile = 'listings.csv'
#xmlFile = 'myDataListings.xml'
csvFile = 'listingsSantaCruz.csv'
xmlFile = 'listingsSantaCruz.xml'

csvData = csv.reader(open(csvFile))
xmlData = open(xmlFile, 'w')
xmlData.write('<?xml version="1.0"?>' + "\n")
# there must be only one top-level tag
xmlData.write('<csv_data>' + "\n")

rowNum = 0
for row in csvData:
    if rowNum == 0:
        tags = row
        # replace spaces w/ underscores in tag names
        for i in range(len(tags)):
            tags[i] = tags[i].replace(' ', '_')
    else: 

        xmlData.write('<entry>' + "\n")
	#xmlData.write('\t'+'<row id ="'+ str(rowNum) + '">' + "\n") 
        for i in range(len(tags)):
            xmlData.write('\t' + '<' + tags[i] + '>"' \
                          + row[i] + '"</' + tags[i] + '>' + "\n")
        xmlData.write('</entry>' + "\n")
            
    rowNum +=1

xmlData.write('</csv_data>' + "\n")
xmlData.close()

