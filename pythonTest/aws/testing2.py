import numpy as np
import cPickle
import sys

fileName = sys.argv[1]

with open(fileName, 'r') as fp:
    for line in fp:
        line = line.replace("\t",",")
        #listingId = line.split(",")[:1]
        line1 = line.split(",")[1:6]
        line2 = map(int, line1)
        print line2
        with open('s3://cs435project/my_SVM_file.pkl','rb') as fid:
            clf = cPickle.load(fid)
        #print "%s   %s" % (listingId ,clf.predict(line2))
        print clf.predict(line2)
