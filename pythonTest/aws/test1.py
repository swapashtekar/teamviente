import numpy as np
import cPickle
import sys

line = sys.argv[1]
line = line.replace("\t",",")
line = map(int, line.split(","))

with open('s3://cs435project/my_SVM_file.pkl' , 'rb') as fid:
    clf = cPickle.load(fid)

print clf.predict(line)
