//package SVMpckg;
import java.io.*;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.Reducer.Context;
import java.util.*;
import java.net.*;
import org.apache.hadoop.fs.*;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.io.*;
import org.apache.hadoop.mapred.*;
import org.apache.hadoop.util.*;


public class SVMReduce extends Reducer<Text,Text,Text,Text>{
	
	
	
	public void reduce(Text key,Iterable<Text> values,Context context)throws IOException,InterruptedException
	{
		for(Text val: values){
			//String InputVal = val.toString();
			String InputVal= val.toString().replaceAll("\t", ",");
			
			//int[] numberArray = new int[InputValArray.length];
			//for(int i=0; i<InputValArray.length;i++){
			//	numberArray[i] = Integer.parseInt(InputValArray[i]);
			//}
			/*String prg = "import numpy as np\nimport cPickle\nwith open('hdfs://des-moines:30825/datafile/my_SVM_file.pkl', 'rb') as fid:\n"
					+"\tclf = cPickle.load(fid)\ntestdata= np.genfromtxt(InputVal, delimiter=\"\t\")"
					+ "\nprint clf.predict(testdata)";
			*/
			
			ProcessBuilder pb = new ProcessBuilder("python","hdfs://rio-grande:30162/dataLoc/test1.py",""+InputVal);
			Process p = pb.start();
			
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String retValue = in.readLine();
			
			/*
			BufferedWriter out = new BufferedWriter(new FileWriter("test1.py"));
			out.write(prg);
			out.close();
			Process p = Runtime.getRuntime().exec("python test1.py ");
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String ret = in.readLine().toString();
			String CSValue = val.toString() + ret;
			*/
			String CSValue = val.toString() + "\t" + retValue;
			context.write(new Text(key), new Text(CSValue));	
			}
				
		}
				
	}
		

